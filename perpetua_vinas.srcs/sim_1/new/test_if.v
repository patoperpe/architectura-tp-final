`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/04/2018 09:05:45 PM
// Design Name: 
// Module Name: test_if
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module test_if #(
    parameter reg_length=32
)();
    reg                                                 clk;
    reg                                                 reset;
    reg                                                 enable;
    
    reg                                                 inst_selector;
    reg     [reg_length-1:0]                            address_branch;
    
    reg                                                 debug;
    reg     [reg_length-1:0]                            debug_address_inst;
    reg     [reg_length-1:0]                            debug_data_inst;
        
    wire    [reg_length-1:0]                            inst_data;
    wire    [reg_length-1:0]                            inst_address;
                
    InstructionFetch #(
        .reg_length(reg_length)
    ) IF (
        //Signals
            .clk(clk),
            .reset(reset),
            .enable(enable),
        
        //Inputs
            //From HazardsUnit
            .inst_selector(inst_selector),
            //From EX
            .address_branch(address_branch),
        
        //Debug inputs
            .debug(debug),
            .debug_address(debug_address_inst),
            .debug_data(debug_data_inst),
        
        //Outputs
            //To ID
            .inst(inst_data),
            //To EX
            .address_mem(inst_address)
    );
    
    initial
    begin
        clk = 0;
        reset = 1;
        enable = 0;
        //Debug mode.
        debug = 1;
        #5
        
        //Stage Enabled.
        reset = 0;
        
        //Write instruction.
        debug_address_inst = 0;
        debug_data_inst = 15;
        #10
        //Write instruction.
        debug_address_inst = 1;
        debug_data_inst = 25;
        #10
        //Write instruction.
        debug_address_inst = 2;
        debug_data_inst = 35;
        #10
        //Write instruction.
        debug_address_inst = 3;
        debug_data_inst = 45;
        #10
        //Write instruction.
        debug_address_inst = 4;
        debug_data_inst = 55;
        #10
        //Write instruction.
        debug_address_inst = 5;
        debug_data_inst = 65;
        #10
        //Write instruction.
        debug_address_inst = 6;
        debug_data_inst = 75;
        #10
        //Read Instruction.
        debug = 0;
        enable = 1;
        reset = 1;
        //Instruction from PC.
        inst_selector = 0;
        address_branch = 2;
        #10
        reset = 0;
        #60
        //Instruction from PC.
        inst_selector = 1;
        #10
        inst_selector = 0;
    end

    always 
        #5 clk = ~clk;
endmodule
