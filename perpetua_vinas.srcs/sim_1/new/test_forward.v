`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2018 05:42:30 PM
// Design Name: 
// Module Name: test_forward
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_forward #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_length)
)();

    reg                       [reg_length-1:0]          inst;
    
    reg                       [bytes_number-1:0]        rs;
    reg                       [bytes_number-1:0]        rt;
    
    reg                       [bytes_number-1:0]        reg_dst_ex_mem;
    reg                                                 reg_write_ex_mem;
    
    reg                       [bytes_number-1:0]        reg_dst_mem;
    reg                                                 reg_write_mem;
    
    wire                       [1:0]                    mux1_alu_selector;
    wire                       [1:0]                    mux2_alu_selector;
    
    ForwardingUnit #(
        .reg_length(reg_length),
        .bytes_number(bytes_number)
    ) forwardingUnit (
        //From ID
            .rs(rs),
            .rt(rt),
            .inst_id(inst),
            
        //From EX
            .reg_dst_ex_mem(reg_dst_ex_mem),
            .reg_write_ex_mem(reg_write_ex_mem),
            
        //From MEM
            .reg_dst_mem(reg_dst_mem),
            .reg_write_mem(reg_write_mem),
            
        //To EX
            .mux1_alu_selector(mux1_alu_selector),
            .mux2_alu_selector(mux2_alu_selector)
    );
    initial
    begin
        rs = 5'b00011;
        rt = 5'b11000;
        inst = 32'b00000000011110000000000000000000;
        reg_dst_ex_mem = 0;
        reg_write_ex_mem = 0;
        reg_dst_mem = 5'b00011;
        reg_write_mem = 5'b11000;
        #5
        inst = 32'b00000000011110000000000000000100;
        #5
        inst = 32'b10000000011110000000000000000000;
        #5
        reg_write_ex_mem = 1;
        #5
        reg_write_ex_mem = 0;
        reg_write_mem = 1;
        #5
        rs = 5'b00000;
        rt = 5'b00000;
    end
endmodule
