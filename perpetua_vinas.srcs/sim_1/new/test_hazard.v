`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2018 05:57:27 PM
// Design Name: 
// Module Name: test_hazard
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_hazard #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_length)
)();

    reg                                                 clk;
    reg                                                 reset;
    reg                                                 enable;
    
    reg                       [reg_length-1:0]          inst;
    reg                                                 mem_rd_id_ex;
    
    reg                       [bytes_number-1:0]        rt;
    
    wire                                                enable_if;
    wire                                                reset_id;
    
    HazardsUnit #(
        .reg_length(reg_length),
        .reg_quantity(reg_quantity),
        .bytes_number(bytes_number)
    ) hazardUnit (
        //Signals
            .clk(clk),
            .reset(reset),
            .enable(enable),
            
        //IF
            //Inputs
                .inst(inst),
            //Outputs
                .enable_if(enable_if),
        //ID
            //Inputs
                .rt_id_ex(rt),
                .mem_rd_id_ex(mem_rd_id_ex),
            //Outputs
                .reset_id(reset_id)
    );
    initial
    begin
        clk = 0;
        reset = 1;
        enable = 0;
        inst = 0;
        mem_rd_id_ex = 0;
        rt = 0;
    #5    
        //Stage Enabled.
        reset = 0;
        enable = 1;
        rt = 5'b10011;
        mem_rd_id_ex = 1;
        inst = 32'b00000010011111110000000000000000;
    #10
        mem_rd_id_ex = 0;
    #10
        mem_rd_id_ex = 1;
        rt = 5'b11111;
    #10
        mem_rd_id_ex = 0;
    #10
        mem_rd_id_ex = 1;
        rt = 5'b11000;
    #10
        mem_rd_id_ex = 0;
    end
    always 
        #5 clk = ~clk;
endmodule