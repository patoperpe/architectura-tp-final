`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2018 12:01:02 PM
// Design Name: 
// Module Name: test_ex
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_ex #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_length)
)();

    reg                                                 clk;
    reg                                                 reset;
    reg                                                 enable;
    
    reg    [reg_length-1:0]                             reg_rt;
    reg    [reg_length-1:0]                             reg_rs;
    reg    [reg_length-1:0]                             sign;
    reg    [4:0]                                        shamt;
    reg    [3:0]                                        alu_selector;
    reg    [bytes_number-1:0]                           reg_dst_id_ex;
    reg                                                 mem_rd_id_ex;
    reg                                                 mem_wr_id_ex;
    reg                                                 reg_write_id_ex;
    reg                                                 halt_inst_id_ex;
    
    reg     [1:0]                                       mux_alu_1;
    reg     [1:0]                                       mux_alu_2;
    reg     [reg_length-1:0]                            reg_data;
    
    wire    [reg_length-1:0]                            alu_result;
    wire    [bytes_number-1:0]                          reg_dest_ex_mem;
    wire                                                mem_rd;
    wire                                                mem_wr;
    wire                                                reg_write_ex_mem;
    wire                                                halt_inst_ex_mem;
                                      
    Execution #(
        .reg_length(reg_length),
        .bytes_number(bytes_number)
    ) EX (
        //Signals
        .clk(clk),
        .reset(reset),
//        .reset(reset | reset_ex),
        .enable(enable),
        
        //Inputs
            //From ID
                .reg_rt(reg_rt),
                .reg_rs(reg_rs),
                .sign(sign),
                .shamt(shamt),
                .reg_dst_in(reg_dst_id_ex),
                .alu_selector(alu_selector),
                
                //Control Signals
                .mem_rd_in(mem_rd_id_ex),
                .mem_wr_in(mem_wr_id_ex),
                .reg_write_in(reg_write_id_ex),
                .halt_inst_in(halt_inst_id_ex),
            //From EX
                .alu_in(alu_result),
                
            //From MEM
                .reg_data(reg_data),
            
            //From ShortCutUnit
                .mux_alu_1(mux_alu_1),
                .mux_alu_2(mux_alu_2),
            
        //Outputs
            //To MEM
            .alu_out(alu_result),
            .alu_zero(),
            .reg_dst_w_out(reg_dest_ex_mem),
            
            .mem_rd_out(mem_rd),
            .mem_wr_out(mem_wr),
            .reg_write_out(reg_write_ex_mem),
            .halt_inst_out(halt_inst_ex_mem)
    );
    
    initial
    begin
        clk = 0;
        reset = 1;
        enable = 0;
        reg_rt = 10;
        reg_rs = 15;
        sign = 20;
        shamt = 25;
        alu_selector = 4'b0011;
        reg_dst_id_ex = 1;
        mem_rd_id_ex = 1;
        mem_wr_id_ex = 1;
        reg_write_id_ex = 1;
        halt_inst_id_ex = 0;
    #15    
        //Stage Enabled.
        reset = 0;
        enable = 1;
    #10
        mux_alu_1 = 0;
        mux_alu_2 = 0;
    #10
        mux_alu_1 = 1;
        mux_alu_2 = 0;
    #10
        mux_alu_1 = 0;
        mux_alu_2 = 1;
    #10
        mux_alu_1 = 1;
        mux_alu_2 = 1;
    end
    always 
        #5 clk = ~clk;
endmodule