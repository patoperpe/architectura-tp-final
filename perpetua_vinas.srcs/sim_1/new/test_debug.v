`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2018 08:28:28 PM
// Design Name: 
// Module Name: test_debug
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_debug #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_length),
    parameter size=64,
    parameter mem_lenght=$clog2(size)
    )();
    
    reg                                                 clk;
    reg                                                 reset;
    reg                                                 enable;
    
    reg                                                 halt_inst;
    
    reg        [7:0]                                    tx_data;
    reg                                                 tx_start;
    
    wire                                                tx_done_tick;
    wire                                                rx_done_tick;
    
    wire                                                tx;
    wire                                                rx;
    
    UART #() uart(
        .reset(reset),
        .clk(clk),
        
        .tx_in(tx_data),
        .tx_start(tx_start),
        
        .tx(tx),
        .rx(rx),
        
        .tx_done_tick(tx_done_tick),
        .rx_done_tick(rx_done_tick)
    );
        
    DebugControl #(
        .reg_length(reg_length),
        .reg_quantity(reg_quantity),
        .bytes_number(bytes_number),
        .size(size),
        .mem_lenght(mem_lenght)
    ) debugUnit (
        //Signals
            .clk(clk),
            .reset_hw(reset),
            .enable_hw(enable),
            
            .enable_master(),
            
            .reset_master(),
            //.reset_if(reset_if),
            //.reset_id(reset_id),
            //.reset_ex(reset_ex),
            //.reset_mem(reset_mem),
            
        //To IF
            .debug(),
            .debug_address_inst(),
            .debug_data_inst(),
        
        //To ID
            .debug_address_reg(),
            .debug_data_reg(),
            
        //To MEM
            .debug_address_mem(),
        //From MEM
            .debug_data_mem(),
        
        //To UART
            .tx(rx),
            .rx(tx),
            
        //From IF
            .inst(),
            .address_pc(),
            
        //From ID
            .inst_id(),
            .rt(),
            .rs(),
            .reg_rt(),
            .reg_rs(),
            .sign(),
            .shamt(),
            .alu_selector(),
            .reg_dst_id_ex(),
            .mem_rd_id_ex(),
            .mem_wr_id_ex(),
            .reg_write_id_ex(),
            .address_branch(),
            
        //From EX
            .alu_result(),
            .mem_rd(),
            .mem_wr(),
            .reg_dst_ex_mem(),
            .reg_write_ex_mem(),
            
        //From MEM
            .reg_data(),
            .reg_dst(),
            .reg_write(),
            .halt_inst(halt_inst),
        .state(),
        .sub_state()
    );
    initial
    begin
        clk <= 0;
        reset <= 0;
        enable <= 0;
        tx_data <= 0;
        tx_start <= 0;
        #10
        reset <= 1;
        #10
        reset <= 0;
        enable <= 1;
        #10
        tx_data <= 8'b00000001;
        tx_start <= 1;
        //Clear register 0
        //ANDI: 0011 0000 0000 0000 0000 0000 0000 0000
        #600000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00000000;
        tx_start <= 1;
        #300000
        tx_data <= 8'b00110000;
        tx_start <= 1;
        //Finish program
        #300000
        tx_data <= 8'b11111111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11111111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11111111;
        tx_start <= 1;
        #300000
        tx_data <= 8'b11111111;
        tx_start <= 1;
        //Continuous signal.
        #300000
        tx_data <= 8'b00000010;
        tx_start <= 1;
        #300000
        halt_inst <= 1;
    end
    always
    begin
        #1 clk = ~clk;
        if(tx == 0 && tx_start == 1)
            tx_start <= 0;
    end
endmodule
