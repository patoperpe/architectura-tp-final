`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:41:44 11/28/2016 
// Design Name: 
// Module Name:    pc 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module pc #(
    parameter reg_length=32
    )(
	input [reg_length-1:0] in,
	input clk,
	input enable,
	input reset,
	output reg [reg_length-1:0] out
    );
	always @(posedge clk)
	begin
		if(enable)
		begin
			out <= in;
		end
		if(reset)
		begin
			out <= 32'b11111111111111111111111111111111;
		end
	end

endmodule
