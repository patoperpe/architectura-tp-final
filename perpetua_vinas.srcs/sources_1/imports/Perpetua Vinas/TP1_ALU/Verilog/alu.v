`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:11:56 09/15/2016 
// Design Name: 
// Module Name:    alu 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module alu #(
    parameter reg_length=32
    )
     (  input     [reg_length-1:0] a,
		input     [reg_length-1:0] b,
		input     [3:0] op,
		output    reg exception,
		output    reg zero,
		output    reg [reg_length-1:0] result);
		
		always @(*)
		begin
            zero = 0;
            exception = 0;
            case({op})
				4'b0000: result =   a <<  b;		//SLL
				4'b0001: result =   a >>  b;		//SRL
				4'b0010: result =   a >>> b;		//SRA
				4'b0011: 
				begin
				    result =   a  +  b;		//ADD
				    //exception = {a  + b, 0};        //Verify overflow
				end
				4'b0100: result =   a  &  b;		//AND
				4'b0101: result =   a  |  b;		//OR
				4'b0110: result =   a  ^  b;		//XOR
				4'b0111: result = ~(a  |  b);		//NOR
				4'b1000:                            //SUB
				begin
				    result = b - a;		
				    zero = a <= b ? 1 : 0;          //Check zeros
				end
				4'b1001:                            //SLT
                begin
                    result = a < b;        
                    zero = a <= b ? 1 : 0;          //Check zeros
                end
				default: 
				begin
				    result = 0;
				    exception = 1;
                end
			endcase
		end
endmodule