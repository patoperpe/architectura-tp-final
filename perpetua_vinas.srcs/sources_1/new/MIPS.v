`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/20/2018 05:00:29 PM
// Design Name: 
// Module Name: MIPS
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MIPS #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_length),
    parameter size=64,
    parameter mem_lenght=$clog2(size)
    )(
    input                                                   clk,
    input                                                   reset_hw,
    input                                                   enable_hw,
    
    input                                                   rx,
    output                                                  tx,
    output                        [5:0]                     state,
    output                        [5:0]                     sub_state
    );
    
    wire                                                    enable;
    
    wire                                                    reset_master;
    wire                                                    reset_if;
    wire                                                    reset_id;
    wire                                                    reset_ex;
    wire                                                    reset_mem;
    
    wire                                                    debug;
    wire                           [reg_length-1:0]         debug_address_inst;
    wire                           [reg_length-1:0]         debug_data_inst;
    wire                           [bytes_number-1:0]       debug_address_reg;
    wire                           [reg_length-1:0]         debug_data_reg;
        
    //From IF
        wire                      [reg_length-1:0]          inst;
        wire                      [reg_length-1:0]          address_pc;
    
    //FROM ID
        //ForwardingUnit
            wire                  [reg_length-1:0]          inst_id;
            wire                  [bytes_number-1:0]        rs;
            wire                  [bytes_number-1:0]        rt;
        //DebugUnit
            wire                  [reg_length-1:0]          reg_rt;
            wire                  [reg_length-1:0]          reg_rs;
            wire                  [reg_length-1:0]          sign;
            wire                  [4:0]                     shamt;
            wire                  [3:0]                     alu_selector;
            wire                  [bytes_number-1:0]        reg_dst_id_ex;
            wire                                            mem_rd_id_ex;
            wire                                            mem_wr_id_ex;
            wire                                            reg_write_id_ex;
            wire                  [reg_length-1:0]          address_branch;
            wire                                            inst_selector;
    
    //FROM EX
        //ForwardingUnit
            wire                  [reg_length-1:0]          alu_result;
            wire                  [bytes_number-1:0]        reg_dst_ex_mem;
            wire                                            reg_write_ex_mem;
        //DebugUnit
            wire                                            mem_rd;
            wire                                            mem_wr;
            
    //From MEM
        //ForwardingUnit
            wire                  [reg_length-1:0]          reg_data_mem;
            wire                  [bytes_number-1:0]        reg_dst_mem;
            wire                                            reg_write_mem;
        //DebugUnit
            wire                                            halt_id;
            wire                                            halt_ex;
            wire                                            halt_mem;
            wire                                            halt_inst;
            wire                   [mem_lenght-1:0]         debug_address_mem;
            wire                   [reg_length-1:0]         debug_data_mem;
            
    //From ForwardingUnit
        wire                      [1:0]                     mux1_alu_selector;
        wire                      [1:0]                     mux2_alu_selector;

    //From HazardsUnit
        wire                                                enable_if;
                
    DebugControl #(
        .reg_length(reg_length),
        .reg_quantity(reg_quantity),
        .bytes_number(bytes_number),
        .size(size),
        .mem_lenght(mem_lenght)
    ) debugUnit (
        //Signals
            .clk(clk),
            .reset_hw(reset_hw),
            .enable_hw(enable_hw),
            
            .enable_master(enable),
            
            .reset_master(reset_master),
            //.reset_if(reset_if),
            //.reset_id(reset_id),
            //.reset_ex(reset_ex),
            //.reset_mem(reset_mem),
            
        //To IF
            .debug(debug),
            .debug_address_inst(debug_address_inst),
            .debug_data_inst(debug_data_inst),
        
        //To ID
            .debug_address_reg(debug_address_reg),
            .debug_data_reg(debug_data_reg),
            
        //To MEM
            .debug_address_mem(debug_address_mem),
            
        //From MEM
            .debug_data_mem(debug_data_mem),
        
        //To UART
            .tx(tx),
            .rx(rx),
            
        //From IF
            .inst(inst),
            .address_pc(address_pc),
            
        //From ID
            .inst_id(inst_id),
            .rt(rt),
            .rs(rs),
            .reg_rt(reg_rt),
            .reg_rs(reg_rs),
            .sign(sign),
            .shamt(shamt),
            .alu_selector(alu_selector),
            .reg_dst_id_ex(reg_dst_id_ex),
            .mem_rd_id_ex(mem_rd_id_ex),
            .mem_wr_id_ex(mem_wr_id_ex),
            .reg_write_id_ex(reg_write_id_ex),
            .address_branch(address_branch),
            .halt_id(halt_id),
            .inst_selector(inst_selector),
            
        //From EX
            .alu_result(alu_result),
            .mem_rd(mem_rd),
            .mem_wr(mem_wr),
            .reg_dst_ex_mem(reg_dst_ex_mem),
            .reg_write_ex_mem(reg_write_ex_mem),
            .halt_ex(halt_ex),
            
        //From MEM
            .reg_data(reg_data_mem),
            .reg_dst(reg_dst_mem),
            .reg_write(reg_write_mem),
            .halt_mem(halt_mem),
            
        //From WR
            .halt_inst(halt_inst),
            
        //From HAZ
            .reset_id(reset_id),
            .enable_if(enable_if),
        
        .state(state),
        .sub_state(sub_state)
    ); 
    
    pipeline #(
        .reg_length(reg_length),
        .reg_quantity(reg_quantity),
        .bytes_number(bytes_number),
        .size(size),
        .mem_lenght(mem_lenght)
    ) pipeUnit (
        //Signals
            .clk(clk),
            
            .enable(enable),
            .enable_if(enable_if),
        
        //ControlUnit    
            .reset(reset_master),
            //.reset_if(reset_if),
            .reset_id(reset_id),
            //.reset_ex(reset_ex),
            //.reset_mem(reset_mem),
            
        //ForwardingUnit
            .mux_alu_1(mux1_alu_selector),
            .mux_alu_2(mux2_alu_selector),
            .inst_id(inst_id),
            
        //DebugUnit
            .debug(debug),
            .debug_address_inst(debug_address_inst),
            .debug_data_inst(debug_data_inst),
            .halt_inst(halt_inst),
            
            .debug_data_reg(debug_data_reg),
            .debug_address_reg(debug_address_reg),
            
            .debug_address_mem(debug_address_mem),
            .debug_data_mem(debug_data_mem),
            
    //Latches
            //IF
                //Outputs
                    .inst(inst),
                    .address_pc(address_pc),
            //ID
                //Outputs
                    .rt(rt),
                    .rs(rs),
                    .reg_rt(reg_rt),
                    .reg_rs(reg_rs),
                    .sign(sign),
                    .shamt(shamt),
                    .alu_selector(alu_selector),
                    .reg_dst_id_ex(reg_dst_id_ex),
                    .mem_rd_id_ex(mem_rd_id_ex),
                    .mem_wr_id_ex(mem_wr_id_ex),
                    .reg_write_id_ex(reg_write_id_ex),
                    .address_branch(address_branch),
                    .halt_id(halt_id),
                    .inst_selector(inst_selector),
            //EX    
                //Outputs
                    .alu_result(alu_result),
                    .mem_rd(mem_rd),
                    .mem_wr(mem_wr),
                    .reg_dest_ex_mem(reg_dst_ex_mem),
                    .reg_write_ex_mem(reg_write_ex_mem),
                    .halt_ex(halt_ex),
            //MEM
                //Outputs
                    .reg_data(reg_data_mem),
                    .reg_dst(reg_dst_mem),
                    .reg_write(reg_write_mem),
                    .halt_mem(halt_mem)
    );
    
    HazardsUnit #(
        .reg_length(reg_length),
        .reg_quantity(reg_quantity),
        .bytes_number(bytes_number)
    ) hazardUnit (
        //Signals
            .clk(clk),
            .reset(reset_master),
            .enable(enable),
            
        //IF
            //Inputs
                .inst(inst),
            //Outputs
                .enable_if(enable_if),
        //ID
            //Inputs
                .rt_id_ex(rt),
                .mem_rd_id_ex(mem_rd_id_ex),
            //Outputs
                .reset_id(reset_id)
    );
    
    ForwardingUnit #(
        .reg_length(reg_length),
        .bytes_number(bytes_number)
    ) forwardingUnit (
        //From ID
            .rs(rs),
            .rt(rt),
            .inst_id(inst_id),
            
        //From EX
            .reg_dst_ex_mem(reg_dst_ex_mem),
            .reg_write_ex_mem(reg_write_ex_mem),
            
        //From MEM
            .reg_dst_mem(reg_dst_mem),
            .reg_write_mem(reg_write_mem),
            
        //To EX
            .mux1_alu_selector(mux1_alu_selector),
            .mux2_alu_selector(mux2_alu_selector)
    );
endmodule
