`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/18/2018 04:18:02 PM
// Design Name: 
// Module Name: control_debug
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DebugControl #(
    parameter reg_length = 32,
    parameter reg_quantity = 32,
    parameter bytes_number = $clog2(reg_length),
    parameter size = 64,
    parameter mem_lenght = $clog2(size)
    )(
    input                                                   clk,
            
    input                                                   reset_hw,
    input                                                   enable_hw,
    
    //From IF
    input                          [reg_length-1:0]         inst,
    input                          [reg_length-1:0]         address_pc,
    
    //FROM ID
    input                           [reg_length-1:0]        inst_id,
    input                           [bytes_number-1:0]      rt,
    input                           [bytes_number-1:0]      rs,
    input                           [reg_length-1:0]        reg_rt,
    input                           [reg_length-1:0]        reg_rs,
    input                           [reg_length-1:0]        sign,
    input                           [4:0]                   shamt,
    input                           [3:0]                   alu_selector,
    input                           [bytes_number-1:0]      reg_dst_id_ex,
    input                                                   mem_rd_id_ex,
    input                                                   mem_wr_id_ex,
    input                                                   reg_write_id_ex,
    input                           [reg_length-1:0]        address_branch,
    input                                                   halt_id,
    input                                                   inst_selector,
    
    //FROM EX
    input                           [reg_length-1:0]        alu_result,
    input                                                   mem_rd,
    input                                                   mem_wr,
    input                           [bytes_number-1:0]      reg_dst_ex_mem,
    input                                                   reg_write_ex_mem,
    input                                                   halt_ex,
    
    //From MEM
    input                           [reg_length-1:0]        reg_data,
    input                           [bytes_number-1:0]      reg_dst,
    input                                                   reg_write,
    input                                                   halt_mem,
    
    //From WB
    input                                                   halt_inst,
    
    //From HAZ
    input                                                   enable_if,
    input                                                   reset_id,
    
    output      reg                                         enable_master,
    
    output      reg                                         reset_master,
    //output      reg                                         reset_if,
    //output      reg                                         reset_id,
    //output      reg                                         reset_ex,
    //output      reg                                         reset_mem,
    
    output      reg                                         debug,
    
    //To IF
    output      reg                  [reg_length-1:0]       debug_address_inst,
    output      reg                  [reg_length-1:0]       debug_data_inst,
    
    //ID TO and FROM, for read registers.
    input                            [reg_length-1:0]       debug_data_reg,
    output      reg                  [bytes_number-1:0]     debug_address_reg,
    
    //MEM TO and FROM for read memory.
    input                            [reg_length-1:0]       debug_data_mem,
    output      reg                  [mem_lenght-1:0]       debug_address_mem,
    
    input                                                   rx,
    output                                                  tx,
    
    output      reg                  [5:0]                  state,
    output      reg                  [5:0]                  sub_state
    );
    
    localparam              [5:0]               IDLE                    = 6'b000001,
                                                PROGRAMMING             = 6'b000010,
                                                WAITING                 = 6'b000100,
                                                STEP_BY_STEP            = 6'b001000,
                                                SENDING_DATA            = 6'b010000,
                                                CONTINUOS               = 6'b100000;

    localparam              [5:0]               SUB_INIT                = 6'b000001,
                                                SUB_READ_1              = 6'b000010,
                                                SUB_READ_2              = 6'b000100,
                                                SUB_READ_3              = 6'b001000,
                                                SUB_READ_4              = 6'b010000,
                                                SUB_WRITE_MEM           = 6'b100000;

    localparam              [7:0]               StartSignal             = 8'b00000001,
                                                ContinuosSignal         = 8'b00000010,
                                                StepByStepSignal        = 8'b00000100,
                                                ReProgramSignal         = 8'b00001000,
                                                StepSignal              = 8'b00010000;

    reg                     [9:0]               counter = 0;
    //reg                     [5:0]               state;
    //reg                     [5:0]               sub_state;
    
    reg                     [reg_length-1:0]    clocks;
    
    
    reg                     [7:0]               reg_counter;
    reg                     [7:0]               bytes_counter;
    
    
    wire                                        rx_done_tick;
    wire                                        tx_done_tick;
    wire                    [7:0]               rx_data;
    reg                                         tx_start = 0;
    reg                     [7:0]               tx_data;
    
    //Data Selection mux (from pipeline signals, latches, memory) 
    wire                    [reg_length-1:0]    data [0:15];
    //IF
    assign                                      data[0] = inst;//Instruction.
    assign                                      data[1] = address_pc;//PC.
    //ID
    assign                                      data[2] = reg_rt;//Target register.
    assign                                      data[3] = reg_rs;//Source register.
    assign                                      data[4] = sign;//Extended signed.
    assign                                      data[5] = {{reg_write_id_ex},{mem_wr_id_ex},{mem_rd_id_ex},{reg_dst_id_ex},{alu_selector},{shamt},{rt},{inst_selector},{halt_id},{rs},3'b000};//ID Latches
    assign                                      data[6] = address_branch;//PC branch
    assign                                      data[7] = inst_id;//Inst ID
    assign                                      data[8] = debug_data_reg;//To read register data
    //EX
    assign                                      data[9] = alu_result;
    assign                                      data[10] = {23'b00000000000000000000000,{halt_ex},{mem_rd},{mem_wr},{reg_dst_ex_mem},{reg_write_ex_mem}}; //Latches EX
    //MEM
    assign                                      data[11] = reg_data;//Data to be write
    assign                                      data[12] = {22'b00000000000000000000,{enable_if},{reset_id},{halt_inst},{halt_mem},{reg_write},{reg_dst}};//Latches MEM
    assign                                      data[13] = debug_data_mem;//To read memory data.
    assign                                      data[14] = clocks; 
    
    wire                    [reg_length-1:0]    data_out;
    reg                     [3:0]               data_selector;
    assign                                      data_out = data[data_selector];
    
    reg                                         halted;
    UART uart(
        .reset(reset_hw),
        .clk(clk),
        
        .tx_in(tx_data),
        .tx_start(tx_start),
        
        .tx(tx),
        .rx(rx),
        
        .rx_out(rx_data),
        
        .tx_done_tick(tx_done_tick),
        .rx_done_tick(rx_done_tick)
    );
    
    always @(posedge clk)
    begin
        if(reset_hw)
        begin
            enable_master <= 0;
            reset_master <= 1;
            debug <= 0;
            debug_address_inst <= 0;
            debug_data_inst <= 0;
            debug_address_reg <= 0;
            state <= IDLE;
            sub_state <= SUB_INIT;
            debug_address_reg <= 0;
            clocks <= 0;
            reg_counter <= 0;
            bytes_counter <= 0;
            tx_start <= 0;
            counter <= 0;
            halted <= 0;
        end
        else if(enable_hw)
        begin
            case(state)
                IDLE:
                begin
                    reset_master <= 0;
                    //index = 0;
                    //reprogram = 0;
                    debug <= 0;
                    if (rx_done_tick) 
                    begin
                        if (rx_data == StartSignal) 
                        begin 
                            if(counter == 651)
                            begin
                                state <= PROGRAMMING;
                                sub_state <= SUB_INIT;
                                counter <= 0;
                            end
                            else
                            begin
                                counter <= counter + 1;
                                if(tx_done_tick)
                                    tx_start <= 0;
                                else 
                                    tx_start <= 1;
                            end
                            tx_data <= StartSignal;
                        end
                        else 
                        begin
                            state <= IDLE;    
                        end
                    end
                end
                PROGRAMMING:
                begin
                    tx_start <= 0;
                    case (sub_state)
                        SUB_INIT:
                            begin
                                if(counter == 651)
                                begin
                                    sub_state <= SUB_READ_1;
                                    counter <= 0;
                                end
                                else
                                    counter <= counter + 1;
                                debug_address_inst <= 0;
                                debug <= 1;
                            end
                        SUB_READ_1:
                            begin
                                debug <= 0;
                                if (rx_done_tick) 
                                begin
                                    debug_data_inst[31:24] <= rx_data;
                                    if(counter == 651)
                                    begin
                                        sub_state <= SUB_READ_2;
                                        counter <= 0;
                                    end
                                    else
                                        counter <= counter + 1;
                                end
                            end
                        SUB_READ_2:
                            begin
                                if (rx_done_tick) 
                                begin
                                    debug_data_inst[23:16] <= rx_data;
                                    if(counter == 651)
                                    begin
                                        sub_state <= SUB_READ_3;
                                        counter <= 0;
                                    end
                                    else
                                        counter <= counter + 1;
                                end
                            end
                        SUB_READ_3:
                            begin
                                if (rx_done_tick) 
                                begin
                                    debug_data_inst[15:8] <= rx_data;
                                    if(counter == 651)
                                    begin
                                        sub_state <= SUB_READ_4;
                                        counter <= 0;
                                    end
                                    else
                                        counter <= counter + 1;
                                end
                            end
                        SUB_READ_4:
                            begin
                                if (rx_done_tick) 
                                begin
                                    debug_data_inst[7:0] <= rx_data;
                                    if(counter == 651)
                                    begin
                                        sub_state <= SUB_WRITE_MEM;
                                        counter <= 0;
                                        debug <= 1;
                                    end
                                    else
                                        counter <= counter + 1;
                                end
                            end
                        SUB_WRITE_MEM:
                            begin
                                debug_address_inst <= debug_address_inst + 1;
                                if (&debug_data_inst[31:0]) 
                                begin
                                    state <= WAITING;
                                    sub_state <= SUB_INIT;
                                    debug <= 0;
                                end
                                else 
                                begin
                                    sub_state <= SUB_READ_1;
                                end
                            end
                    endcase 
                end
                WAITING:
                begin
                    clocks = 0;
                    reset_master = 1;
                    if (rx_done_tick) 
                    begin
                        case (rx_data)
                            ReProgramSignal: 
                            begin
                                state <= IDLE;                                                        
                            end
                            ContinuosSignal: 
                            begin 
                                state <= CONTINUOS;
                                reset_master <= 0;
                                enable_master <= 1;
                                debug <= 0;
                                halted <= 0;
                            end 
                            StepByStepSignal: 
                            begin 
                                state <= STEP_BY_STEP;
                                reset_master <= 0;
                                enable_master <= 0;
                                debug <= 0;
                                halted <= 0;
                            end
                        endcase                    
                    end
                end
                STEP_BY_STEP:
                begin
                    enable_master = 0;
                    if (rx_done_tick) 
                    begin
                        if (rx_data == StepSignal) 
                        begin
                            enable_master <= 1;
                            clocks <= clocks + 1;
                            debug <= 0;
                            data_selector <= 0;
                            debug_address_mem <= 0;
                            debug_address_reg <= 0;
                            sub_state <= SUB_READ_1;
                            state <= SENDING_DATA;
                        end
                    end
                end
                CONTINUOS:
                begin
                    if (halt_inst) 
                    begin
                        data_selector <= 0;
                        debug_address_mem <= 0;
                        debug_address_reg <= 0;
                        sub_state <= SUB_READ_1;
                        state <= SENDING_DATA;
                        halted <= 1;
                        enable_master <= 0;
                    end
                    else 
                    begin
                        clocks <= clocks + 1;
                    end
                end
                SENDING_DATA:
                begin
                    enable_master <= 0;
                    debug <= 1;
                    if(tx_done_tick)//If a shipping was finished
                    begin
                        if(counter == 651)
                        begin
                            counter <= 0;
                            tx_start <= 0;//Do not send anymore.
                            if(sub_state == SUB_READ_4)
                            begin
                                sub_state <= SUB_READ_1;
                                case(data_selector)
                                    8://Sending registers
                                    begin
                                        if(debug_address_reg == reg_quantity-1)
                                        begin
                                            data_selector <= data_selector + 1;
                                        end
                                        else
                                        begin
                                            debug_address_reg <= debug_address_reg + 1;
                                        end
                                    end
                                    13://Sending memory
                                    begin
                                        if(debug_address_mem == size-1)
                                        begin
                                            data_selector <= data_selector + 1;
                                        end
                                        else
                                        begin
                                            debug_address_mem <= debug_address_mem + 1;
                                        end
                                    end
                                    14:
                                    begin
                                        if(halt_inst || halted)
                                        begin
                                            state <= WAITING;
                                            clocks <= 0;
                                            counter <= 0;
                                        end
                                        else
                                        begin
                                            state <= STEP_BY_STEP;
                                        end
                                    end
                                    default:
                                    begin
                                        data_selector <= data_selector + 1;
                                    end
                                endcase
                            end
                            else
                            begin
                                sub_state <= sub_state << 1;
                            end
                        end
                        else
                        begin
                            counter <= counter + 1;
                        end
                    end
                    else
                    begin
                        tx_start <= 1;
                    end
                    case(sub_state)
                        SUB_READ_1:
                        begin
                            tx_data <= data_out[7:0];
                        end
                        SUB_READ_2:
                        begin
                            tx_data <= data_out[15:8];
                        end
                        SUB_READ_3:
                        begin
                            tx_data <= data_out[23:16];
                        end
                        SUB_READ_4:
                        begin
                            tx_data <= data_out[31:24];
                        end
                    endcase
                end
            endcase
        end
    end
endmodule
