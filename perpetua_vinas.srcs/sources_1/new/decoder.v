`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/17/2018 02:27:54 PM
// Design Name: 
// Module Name: decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module decoder #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_quantity),
	parameter len_exec_bus = 11,
	parameter len_mem_bus = 9,
	parameter len_wb_bus = 2
	)(
	
	input                                                   clk,
        
    input                                                   reset,
    input                                                   enable,
    
	input                           [reg_length-1:0]        inst,
	input                           [reg_length-1:0]        address_pc,
    input                                                   zero,
    
    output  reg                     [3:0]                   alu_selector,
    
    output  reg                                             mux_reg_dst,
    
    output  reg                                             mux_inst,
    
    output  reg                                             mem_rd,
    output  reg                                             mem_wr,
    
    output  reg                                             reg_write,
    
    output  reg                                             mux_branch,
    output  reg                                             halt_inst,
    output  reg                     [reg_length-1:0]        inst_out,
    output  reg                                             reg_dst_selector
    );        

always @(posedge clk)
begin
    if(reset)
    begin
        alu_selector <= 4'b1111;
        mux_reg_dst <= 1;//By default rt
        mux_inst <= 0; //By default PC.
        mem_rd <= 0; //By default read disabled.
        mem_wr <= 0; //By default write disabled.
        reg_write <= 0; //By default write register disabled.
        halt_inst <= 0;
        inst_out <= 0;
        mux_branch <= 0;
        halt_inst <= 0; //Halt Instruction.
    end
    else if(enable)
    begin
        inst_out <= inst;
        if(&inst && &address_pc == 0)
            halt_inst <= 1; //Halt Instruction.
        else
            halt_inst <= 0; //Halt Instruction.
        case(inst[31:29])
                3'b000: 
                begin 
                    mux_reg_dst <= 0;//Out rd
                    reg_dst_selector <= 1;
                    case(inst[28:26])
                    3'b000://R-type
                    begin
                        case(inst[5:0])
                            6'b000000: alu_selector <= 4'b0000;
                            6'b000010: alu_selector <= 4'b0001;
                            6'b000011: alu_selector <= 4'b0010;
                            6'b000100: alu_selector <= 4'b0000;
                            6'b000110: alu_selector <= 4'b0001;
                            6'b000111: alu_selector <= 4'b0010;
                            6'b100001: alu_selector <= 4'b0011;
                            6'b100011: alu_selector <= 4'b1000;
                            6'b100100: alu_selector <= 4'b0100;
                            6'b100101: alu_selector <= 4'b0101;
                            6'b100110: alu_selector <= 4'b0110;
                            6'b100111: alu_selector <= 4'b0111;
                            6'b101010: alu_selector <= 4'b1001;
                            default:   alu_selector <= 4'b1111;
                        endcase
                        case(inst[5:0])
                            6'b001000, 6'b001001: reg_write <= 0;
                            default: reg_write <= 1; 
                        endcase
                        mux_inst <= 0; //By default PC.
                        mem_rd <= 0; //By default read disabled.
                        mem_wr <= 0; //By default write disabled.
                        mux_branch <= 0;//Default branch pc.
                    end
                    3'b100,3'b101://Branch
                    begin
                        if((zero && !inst[26]) || (!zero && inst[26]))
                        begin
                            mux_inst <= 1; //branch address.
                        end
                        else
                        begin
                            mux_inst <= 0; //Default PC.
                        end
                        mem_rd <= 0; //By default read disabled.
                        mem_wr <= 0; //By default write disabled.
                        reg_write <= 0; //By default write register disabled.
                        mux_branch <= 0;//Default branch pc.
                    end
                    default://Jump
                    begin
                        mux_inst <= 1; //Branch address.
                        mem_rd <= 0; //By default read disabled.
                        mem_wr <= 0; //By default write disabled.
                        reg_write <= 0; //By default write register disabled.
                        mux_branch <= 1;//PC jump address.
                    end
                    endcase
                end
                3'b100,//Loads
                3'b101://Stores 
                begin
                    alu_selector <= 4'b0011;
                    if(inst[29])
                    begin
                        mem_rd <= 0;
                        mem_wr <= 1;
                        reg_write <= 0; //By default write register disabled.
                    end
                    else
                    begin
                        mem_rd <= 1;
                        mem_wr <= 0; 
                        reg_write <= 1; //Write register with memory data.
                    end
                    
                    mux_inst <= 0; //By default PC.
                    mux_reg_dst <= 1;//By default rt
                    mux_branch <= 0;//Default branch pc.
                    reg_dst_selector <= 0;
                end
                3'b001://Inmediates
                begin
                    case(inst[28:26])
                        3'b000: alu_selector <= 4'b0011;
                        3'b100: alu_selector <= 4'b0100;
                        3'b101: alu_selector <= 4'b0101;
                        3'b110: alu_selector <= 4'b0110;
                        3'b111: alu_selector <= 4'b0000;
                        3'b010: alu_selector <= 4'b1001;
                        default: alu_selector <= 4'b1111;
                    endcase
                    reg_write <= 1;
                    mux_reg_dst <= 1;//By default rt
                    mux_inst <= 0; //By default PC.
                    mem_rd <= 0; //By default read disabled.
                    mem_wr <= 0; //By default write disabled.
                    mux_branch <= 0;//Default branch pc.
                    reg_dst_selector <= 1;//Save ALU result into reg.
                end        
                default:
                begin
                    alu_selector <= 4'b1111;
                    mux_reg_dst <= 1;//By default rt
                    mux_inst <= 0; //By default PC.
                    mem_rd <= 0; //By default read disabled.
                    mem_wr <= 0; //By default write disabled.
                    reg_write <= 0; //By default write register disabled.
                    mux_branch <= 0;//Default branch pc.
                end
            endcase
    end
end 

/*always @(*)
begin
    if(reset)
    begin
        mux_inst <= 0;
    end
    if(enable)
    begin
        if((inst[31:26] == 6'b000100 || inst[31:26] == 6'b000101) && zero)
            mux_inst <= 1;
        else
            mux_inst <= 0;
    end
end*/
endmodule
