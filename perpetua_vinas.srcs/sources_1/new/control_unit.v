`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/18/2018 04:17:09 PM
// Design Name: 
// Module Name: control_unit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module HazardsUnit #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_quantity)
    )(
    
    input                                                   clk,
    input                                                   reset,
    
    input                                                   enable,
    
    //From IF
    input                           [reg_length-1:0]        inst,
    
    //From ID
    input                           [bytes_number-1:0]      rt_id_ex,
    input                                                   mem_rd_id_ex,
    
    
    //To IF
    output reg                                              enable_if,
    
    //To ID
    output reg                                              reset_id
    );
    
    always @(negedge clk)
    begin
       if(reset)
        begin
            enable_if <= 0;
            reset_id <= 0;
        end
        else if(enable)
        begin
            if(mem_rd_id_ex && (inst[25:21] == rt_id_ex || inst[20:16] == rt_id_ex))
             begin
                enable_if <= 0;
                 reset_id <= 1;
             end
             else
             begin
                enable_if <= 1;
                 reset_id <= 0;
             end
        end
        
    end
endmodule
