`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/14/2018 03:25:45 PM
// Design Name: 
// Module Name: WriteBack
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MemoryAccess #(
    parameter reg_length=32,
    parameter bytes_number = $clog2(reg_length),
    parameter size=64,
    parameter mem_lenght=$clog2(size)
    )(
    input                                                   clk,
            
    input                                                   reset,
    input                                                   enable,
    
    input               [reg_length-1:0]                    alu_result,
    
    input                                                   reg_dst_selector,
    input               [reg_length-1:0]                    reg_data_in,
    input               [bytes_number-1:0]                  reg_dst_in,
    input                                                   reg_write_in,
       
    input                                                   mem_rd,
    input                                                   mem_wr,
    
    input                                                   halt_inst_in,
    
    output              [reg_length-1:0]                    reg_data_out,
    output  reg         [bytes_number-1:0]                  reg_dst_out,
    output  reg                                             reg_write_out,
    
    //To Debug
    input                                                   debug,
    input               [mem_lenght-1:0]                    debug_address,
    output              [reg_length-1:0]                    debug_data,
    output  reg                                             halt_inst_out          
    );

    wire                [reg_length-1:0]                    mux_memory;
    
    reg                 [reg_length-1:0]                    alu_out;
    reg                                                     mux_dst_data;
    
    mux #(
        .reg_length(reg_length)
    ) mux_1 (
        .in1(mux_memory),
        .in2(alu_out),
        .out(reg_data_out),
        .selector(mux_dst_data)
    );
    
    memory #(
        .reg_length(reg_length),
        .mem_lenght(mem_lenght),
        .size(size)
    ) memory(
        .clk(clk),
        .enable(enable && !halt_inst_in),
        .reset(reset),
        
        .rd(mem_rd),
        .wr(mem_wr && !halt_inst_in),
        
        .debug(debug),
        .debug_address(debug_address),
        .debug_data_out(debug_data),
        
        .addr(alu_result[5:0]),
        .out(mux_memory),
        .in(reg_data_in)
    );
        
    always @(posedge clk)
    begin
        if(reset)
        begin
            reg_dst_out <= 0;
            reg_write_out <= 0;
            alu_out <= 0;
            mux_dst_data <= 0;
            halt_inst_out <= 0;
        end
        else if(enable)//&& !halt_inst_in)
        begin
            halt_inst_out <= halt_inst_in;
            mux_dst_data <= reg_dst_selector;
            alu_out <= alu_result;
            reg_dst_out <= reg_dst_in;
            reg_write_out <= reg_write_in;
        end
        //halt_inst_out <= halt_inst_in;
    end
endmodule
