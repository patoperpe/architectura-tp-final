`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/17/2018 04:58:05 PM
// Design Name: 
// Module Name: spliter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module spliter #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_quantity)
	)(
    input                                                   clk,
            
    input                                                   reset,
    input                                                   enable,
	
	input                           [reg_length-1:0]        inst,
    
    output  reg                     [reg_length-1:0]        out_sign,    
    output  reg                     [bytes_number-1:0]      out_rt,
    output  reg                     [bytes_number-1:0]      out_rd,
    output  reg                     [bytes_number-1:0]      out_rs,
    output  reg                     [bytes_number-1:0]      out_shamt,
    output  reg                     [reg_length-1:0]        address_jump
    );
    
    always @(posedge clk)//, posedge reset) 
    begin
        if (reset) 
        begin
            out_sign <= 0;
            out_rt <= 0;
            out_rd <= 0;
            out_rs <= 0;
            out_shamt <= 0;
            address_jump <= 0;
        end
        else if(enable)
        begin
            out_sign <= $signed(inst[15:0]);
            out_rt <= inst[20:16];
            out_rs <= inst[25:21];
            out_rd <= inst[15:11];
            out_shamt <= inst[10:6];
            address_jump <= inst[25:0];            
        end
    end
    
endmodule
