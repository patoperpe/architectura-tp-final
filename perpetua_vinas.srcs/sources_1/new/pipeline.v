`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/19/2018 01:03:45 PM
// Design Name: 
// Module Name: pipeline
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pipeline #(
    parameter reg_length=32,
    parameter reg_quantity=32,
    parameter bytes_number = $clog2(reg_length),
    parameter size=64,
    parameter mem_lenght=$clog2(size)
    )(
    input                                                   clk,
    
    input                                                   reset,
//    input                                                   reset_if,
    input                                                   reset_id,
//    input                                                   reset_ex,
//    input                                                   reset_mem,
    
    input                                                   enable,
    input                                                   enable_if,
        
    input                                                   debug,
    input                           [reg_length-1:0]        debug_address_inst,
    input                           [reg_length-1:0]        debug_data_inst,
    
    input                           [mem_lenght-1:0]        debug_address_mem,
    
    input                           [bytes_number-1:0]      debug_address_reg,
        
    input                           [1:0]                   mux_alu_1,
    input                           [1:0]                   mux_alu_2,
    
    //From IF
    output                          [reg_length-1:0]        inst,
    output                          [reg_length-1:0]        address_pc,
    
    //FROM ID
    output                          [reg_length-1:0]        inst_id,
    output                          [bytes_number-1:0]      rt,
    output                          [bytes_number-1:0]      rs,
    output                          [reg_length-1:0]        reg_rt,
    output                          [reg_length-1:0]        reg_rs,
    output                          [reg_length-1:0]        sign,
    output                          [4:0]                   shamt,
    output                          [3:0]                   alu_selector,
    output                          [bytes_number-1:0]      reg_dst_id_ex,
    output                                                  mem_rd_id_ex,
    output                                                  mem_wr_id_ex,
    output                                                  reg_write_id_ex,
    output                          [reg_length-1:0]        address_branch,
    output                          [reg_length-1:0]        debug_data_reg,
    output                                                  halt_id,
    output                                                  inst_selector,
    
    //FROM EX
    output                          [reg_length-1:0]        alu_result,
    output                                                  mem_rd,
    output                                                  mem_wr,
    output                          [bytes_number-1:0]      reg_dest_ex_mem,
    output                                                  reg_write_ex_mem,
    output                                                  halt_ex,
    
    //From MEM
    output                          [reg_length-1:0]        reg_data,
    output                          [bytes_number-1:0]      reg_dst,
    output                                                  reg_write,
    output                                                  halt_mem,
    
    //From WR
    output                                                  halt_inst,
    
    output              [reg_length-1:0]                    debug_data_mem
    ); 
    
    wire                                                    w_inst_selector;
    
    //wire                                                    halt_inst_id_ex;
    //wire                                                    halt_inst_mem_id;
    
    wire                         [reg_length-1:0]           w_reg_rt_out;
    wire                                                    w_reg_dst_selector_id_ex;
    wire                                                    w_reg_dst_selector_ex_mem;
    
    InstructionFetch #(
        .reg_length(reg_length)
    ) IF (
        //Signals
            .clk(clk),
//            .reset(reset | reset_if),
            .reset(reset),
            .enable(enable & enable_if),
        
        //Inputs
            //From HazardsUnit
            .inst_selector(inst_selector),
            //From EX
            .address_branch(address_branch),
        
        //Debug inputs
            .debug(debug),
            .debug_address(debug_address_inst),
            .debug_data(debug_data_inst),
        
        //Outputs
            //To ID
            .inst(inst),
            .inst_selector_out(w_inst_selector),
            
            //To EX
            .address_mem(address_pc)
    );
    
    InstructionDecoder #(
        .reg_length(reg_length),
        .reg_quantity(reg_quantity),
        .bytes_number(bytes_number)
    ) ID (
        //Signals
            .clk(clk),
            .reset(reset | reset_id | w_inst_selector),//Reset if branch or jump
            .enable(enable),
        
        //Inputs 
            //From DebugUnit
                .debug(debug),
                .debug_address(debug_address_reg),
                .debug_data(debug_data_reg),
            //From IF
                .inst(inst),
                .address_pc(address_pc),
            //From MEM
                .reg_write(reg_write),
                .reg_dst(reg_dst),
                .reg_data(reg_data),
                .halt_mem(halt_mem),
                .halt_wr(halt_inst),
            
        //Outputs
        
            //To Forwarding Unit
                .inst_out(inst_id),
                .rt(rt),
                .rs(rs),
            //To IF
                .address_branch(address_branch),
                .inst_selector(inst_selector),
                
            //To EX
                .alu_selector(alu_selector),
                .reg_rt(reg_rt),
                .reg_rs(reg_rs),        
                .out_sign(sign),
                .out_shamt(shamt),
                .mem_rd(mem_rd_id_ex),
                .mem_wr(mem_wr_id_ex),
                .reg_write_out(reg_write_id_ex),
                .halt_inst(halt_id),
                .reg_dst_selector(w_reg_dst_selector_id_ex),
                
            //To MEM
                .reg_dst_out(reg_dst_id_ex)
    );
    
    Execution #(
        .reg_length(reg_length),
        .bytes_number(bytes_number)
    ) EX (
        //Signals
        .clk(clk),
        .reset(reset),
//        .reset(reset | reset_ex),
        .enable(enable),
        
        //Inputs
            //From ID
                .reg_rt(reg_rt),
                .reg_rs(reg_rs),
                .sign(sign),
                .shamt(shamt),
                .reg_dst_in(reg_dst_id_ex),
                .alu_selector(alu_selector),
                
                //Control Signals
                .mem_rd_in(mem_rd_id_ex),
                .mem_wr_in(mem_wr_id_ex),
                .reg_write_in(reg_write_id_ex),
                .halt_inst_in(halt_id),
                .reg_dst_selector_in(w_reg_dst_selector_id_ex),
            //From EX
                .alu_in(alu_result),
                
            //From MEM
                .reg_data(reg_data),
            
            //From ShortCutUnit
                .mux_alu_1(mux_alu_1),
                .mux_alu_2(mux_alu_2),
            
        //Outputs
            //To MEM
            .alu_out(alu_result),
            .alu_zero(),
            .reg_dst_w_out(reg_dest_ex_mem),
            .reg_rt_out(w_reg_rt_out),
            
            .mem_rd_out(mem_rd),
            .mem_wr_out(mem_wr),
            .reg_write_out(reg_write_ex_mem),
            .halt_inst_out(halt_ex),
            .reg_dst_selector_out(w_reg_dst_selector_ex_mem)
    );
    
    MemoryAccess #(
        .reg_length(reg_length),
        .bytes_number(bytes_number),
        .size(size),
        .mem_lenght(mem_lenght)
    ) MEM (
        //Signals
        .clk(clk),
        .reset(reset),
//        .reset(reset | reset_mem),
        .enable(enable),
        .debug(debug),
        
        //Inputs
            //From EX
                .alu_result(alu_result),
                
                .reg_dst_in(reg_dest_ex_mem),
                .reg_data_in(w_reg_rt_out),
                .reg_write_in(reg_write_ex_mem),
                
                .mem_rd(mem_rd),
                .mem_wr(mem_wr),
                .halt_inst_in(halt_ex),
                .reg_dst_selector(w_reg_dst_selector_ex_mem),
            //From Debug
                .debug_address(debug_address_mem),
                
        //Outputs
            //To DE
            .reg_write_out(reg_write),
            .reg_dst_out(reg_dst),
            .reg_data_out(reg_data),
            
            //To DebugUnit
            .debug_data(debug_data_mem),
            .halt_inst_out(halt_mem)
    );
endmodule
