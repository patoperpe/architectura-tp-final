`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/14/2018 03:25:45 PM
// Design Name: 
// Module Name: InstructionFetch
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module InstructionFetch #(
    parameter reg_length=32
    )(
    input                                                   clk,
    
    input                                                   reset,
    input                                                   enable,
    
    input                                                   inst_selector,
    input                           [reg_length-1:0]        address_branch,
    
    input                                                   debug,
    input                           [reg_length-1:0]        debug_address,
    input                           [reg_length-1:0]        debug_data,
    
    output                          [reg_length-1:0]        inst,
    
    output  reg                                             inst_selector_out,
    
    output                          [reg_length-1:0]        address_mem
    );
    
    wire [reg_length-1:0] mux_pc;
    wire [reg_length-1:0] mux_alu;
    wire [reg_length-1:0] pc_mem;
    
    wire [reg_length-1:0] mux_mem;
    
    mux #(
        .reg_length(reg_length)   
    ) mux_1(
        .selector(inst_selector),
        .in1(mux_alu),
        .in2(address_branch),
        .out(mux_pc)
    );
    
    pc #(
        .reg_length(reg_length)   
    ) pc_1(
        .in(mux_pc),
        .clk(clk),
        .reset(reset),
        .out(address_mem),
        .enable(enable)
    ); 
    
    alu #(
        .reg_length(reg_length)
    ) alu_1(
        .a(address_mem),
        .b(1),
        .op(3),
        .result(mux_alu)
    );
    
    memory #(
        .reg_length(reg_length)
    ) mem (
        .clk(clk),
        .enable(enable),
        .reset(reset),
        .rd(!debug),
        .wr(debug),
        .addr(mux_pc),
        .out(inst),
        .debug_address(debug_address),
        .debug(debug),
        .debug_data_in(debug_data)
    );
    
    always @(negedge clk)
    begin
        if(reset)
            inst_selector_out <= 0;
        if(enable)
            inst_selector_out <= inst_selector;
    end
endmodule
