# Architecture Final Project (Perpetua Viñas)

## Contributing

----------------------

Contributions to this repository are very welcome.

To contribute, please fork this repository on GitLab and send a pull request with a clear description of your changes. If appropriate, please ensure that the user documentation in this README is updated.

If you have submitted a PR and not received any feedback for a while, feel free to [ping us on Twitter](https://twitter.com/patoperpetua) [or find us on facebook](https://www.facebook.com/pato.arg)

----------------------

## TODO

* [ ] SetUp CI/CD

----------------------

© [Perpetua Patricio](http://patricioperpetua.singleton.com.ar), [Laboratorio de Arquitectura de Computadoras, Facultad de Ciencias Exactas Físicas y Naturales, Universidad Nacional de Cordoba](http://www.dep.computacion.efn.uncor.edu/), Argentina, 2018.
